import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class AutoVender {
    private JButton button1;
    private JTextPane textPane1;
    private JTextPane textPane2;
    private JPanel root;
    private JButton button2;
    private JButton chocominticeButton;
    private JButton zenzaiiceButton;
    private JButton pieButton;
    private JButton sirasugohanButton;
    private JButton checkOutButton;
    private JButton cancelButton;
    static int total=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("AutoVender");
        frame.setContentPane(new AutoVender().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    int orderConfirmation(String food,int y,int sum){
        int goukei=0;
        int confirmation =JOptionPane.showConfirmDialog(null,
                "would you like to order "+food+"?",
                "order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            JOptionPane.showMessageDialog(null, "order for " + food + " received");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food +" "+y+ "yen\n");
            goukei= sum+y;
            textPane2.setText("Total with tax"+goukei*11/10+" yen\nTax excluded "+goukei+" yen");
            //上記の税率計算では、小数点以下の値は切り捨てられる。
            // 調べてみると、税金計算の際に出た端数の処理は、各事業者の自由であるらしい。
            // だから、このプログラムでは、切り捨てているのだということを知っておくだけで良い。
        }
        else{
            goukei=sum;
        }
        return goukei;
    }
    int checkOut(int sum){
        int confirmation =JOptionPane.showConfirmDialog(null,
                "would you like to checkout?\nYou have ordered the following dishes.\n\n"+textPane1.getText(),
                "checkout Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            textPane1.setText(null);
            textPane2.setText(null);
            JOptionPane.showMessageDialog(null, "Thank you. The total price with tax is " + sum*11/10 + " yen.");
            //上の税率計算と同様
            return 0;
        }
        else{
            return sum;
        }

    }

    int cancel(int sum){
        int confirmation =JOptionPane.showConfirmDialog(null,
                "would you like to cancel?",
                "cancel",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            textPane1.setText(null);
            textPane2.setText(null);
            JOptionPane.showMessageDialog(null, "Canceled");
            //上の税率計算と同様
            return 0;
        }
        else{
            return sum;
        }
    }

    public AutoVender() {
        
        button1.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("Hamburger.png"))));
        button1.setText("");
        System.out.println();
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total= orderConfirmation("Hamburger",200,total);

            }
        });

        button2.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("Sweetpotatoice.JPG"))));
        button2.setText("");
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=orderConfirmation("Sweetpotatoice",150,total);
            }
        });

        chocominticeButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("Chocomintice.JPG"))));
        chocominticeButton.setText("");
        chocominticeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=orderConfirmation("chocomintice",300,total);
            }
        });


        zenzaiiceButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("Zenzaiice.JPG"))));
        zenzaiiceButton.setText("");
        zenzaiiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=orderConfirmation("ice parfait",400,total);
            }
        });

        checkOutButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("check out.png"))));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=checkOut(total);
            }
        });

        pieButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("pie.JPG"))));
        pieButton.setText("");
        pieButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=orderConfirmation("pie",450,total);
            }
        });

        sirasugohanButton.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("sirasu gohan.JPG"))));
        sirasugohanButton.setText("");
        sirasugohanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=orderConfirmation("sirasu gohan",800,total);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                total=cancel(total);
            }
        });
    }
}
